#!/bin/bash

sed -n '/ image:/p' 02.application-template/deploy.yaml  
sed -n '/ image:/p' 03.workbench-job-management/deployment.yaml
sed -n '/ image:/p' 04.hpc-job-management/deployment.yaml
sed -n '/ image:/p' 05.science-application-management/deploy.yaml
sed -n '/ image:/p' 06.front/deploy.yaml
sed -n '/ image:/p' 06.front/deploy-project.yaml
sed -n '/ image:/p' 06.front/deploy-scapp.yaml
sed -n '/ image:/p' 06.front/deploy-workbench.yaml
sed -n '/ image:/p' 06.front/deploy-contents.yaml
sed -n '/ image:/p' 08.notebook_fastapi/deploy.yaml
sed -n '/ image:/p' 09.kubernetes-workload-management/deploy.yaml
sed -n '/ image:/p' key-cloak/deployment.yaml
