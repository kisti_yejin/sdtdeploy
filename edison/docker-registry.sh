#!/bin/bash

kubectl delete secret -n edison-v2 docker-registry-login

kubectl create secret generic docker-registry-login -n edison-v2 \
--from-file=.dockerconfigjson=/home/dev/.docker/config.json \
--type=kubernetes.io/dockerconfigjson

